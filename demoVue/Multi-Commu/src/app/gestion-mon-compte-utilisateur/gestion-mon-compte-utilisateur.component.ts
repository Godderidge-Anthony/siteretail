import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UtilisateurService } from '../service/utilisateur.service';
import { UtilisateurDto } from '../model/utilisateur-dto';

@Component({
  selector: 'app-gestion-mon-compte-utilisateur',
  templateUrl: './gestion-mon-compte-utilisateur.component.html',
  styleUrls: ['./gestion-mon-compte-utilisateur.component.css']
})
export class GestionMonCompteUtilisateurComponent implements OnInit {
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  utilisateur: UtilisateurDto;
  age: number;
  constructor(private router: Router,private utilisateurService: UtilisateurService ) { }

  ngOnInit() {
   

    this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
      util => {
        this.utilisateur = util;
        console.log("le nom du user est "+this.utilisateur.nom)
        console.log(this.utilisateur.dateAge);
        var timeDiff = Date.now() - new Date(this.utilisateur.dateAge).getTime();
        this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
        console.log(this.age);
      }
    );
  

  }

}