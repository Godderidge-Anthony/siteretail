import { Component, OnInit, Input } from '@angular/core';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';

@Component({
  selector: 'app-gestion-mon-compte-detail',
  templateUrl: './gestion-mon-compte-detail.component.html',
  styleUrls: ['./gestion-mon-compte-detail.component.css']
})
export class GestionMonCompteDetailComponent implements OnInit {
  
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  utilisateur: UtilisateurDto=new UtilisateurDto();
  age: number;

  constructor(private utilisateurService: UtilisateurService) { }

  ngOnInit() {
    
    console.log("url "+this.objJson.id)
    this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
      util => {
        this.utilisateur = util;
        this.utilisateurService.subjectMiseAJour.next();
        console.log(this.utilisateur.dateAge);
        var timeDiff = Date.now() - new Date(this.utilisateur.dateAge).getTime();
        this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
        console.log(this.age);
      }
    );
  }
  }
