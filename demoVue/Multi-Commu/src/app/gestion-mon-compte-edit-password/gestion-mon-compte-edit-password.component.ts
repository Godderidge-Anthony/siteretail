import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../must-match/must-match.validator';
import { UtilisateurService } from '../service/utilisateur.service';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gestion-mon-compte-edit-password',
  templateUrl: './gestion-mon-compte-edit-password.component.html',
  styleUrls: ['./gestion-mon-compte-edit-password.component.css']
})
export class GestionMonCompteEditPasswordComponent implements OnInit {
  
  registerForm: FormGroup;
  submitted = false;
  utilisateur: UtilisateurDto;
  test: any;

  constructor(private utilisateurService: UtilisateurService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    
    this.test = JSON.parse(localStorage.getItem('current_user'));
    this.utilisateurService.getUtilisateur(this.test.id).subscribe(
      res => {
        this.utilisateur = res;
        console.log(this.utilisateur.password);
      }
    );
    this.registerForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(255), Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
      this.updateUtilisateur();
      console.log(this.utilisateur.password);
      // display form values on success
      alert('Votre mot de passe a bien été modifié.');
      this.router.navigate(['mon-compte']);

    }
  }


  updateUtilisateur() {
    this.utilisateurService.updateUtilisateur(this.utilisateur)
      .subscribe(data => {

      },
        error => console.log(error));

  }



  onReset() {
    this.submitted = false;
    this.registerForm.reset();
    this.router.navigate(['mon-compte']);
  }
}
