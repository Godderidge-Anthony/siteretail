import { CategoryProduitdto } from "./categoryProduit-dto";

export class ProduitDto {
    id: number;
    nom: string;
    description: string;
    prix: number;
    quantite: number;
    categoryProduitDto: CategoryProduitdto;
    
    
    

    constructor(id?: number, nom?: string, description?: string, prix?: number, quantite?: number, typeDeProduit?: CategoryProduitdto) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.prix = prix;
        this.quantite = quantite;
        this.categoryProduitDto = typeDeProduit;
       
       
        

    }
}
