import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faEye, faToggleOn } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { CategoryProduitdto } from '../model/categoryProduit-dto';
import { ProduitDto } from '../model/produit-dto';
import { AlertService } from '../service/alert.service';
import { CategoryProduitService } from '../service/categoryProduit.service';
import { ProduitService } from '../service/produit.service';

@Component({
  selector: 'app-produit-list',
  templateUrl: './produit-list.component.html',
  styleUrls: ['./produit-list.component.css']
})
export class ProduitListComponent implements OnInit {
  produits: Observable<ProduitDto[]>;

  constructor(private produitService: ProduitService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.produits = this.produitService.getProduitList();
  }

  deleteProduit(id: number) {
    this.produitService.deleteProduit(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  produiteDetails(id: number){
    this.router.navigate(['details', id]);
  }

  updateProduit(id: number){
    this.router.navigate(['update', id]);
  }
}

  