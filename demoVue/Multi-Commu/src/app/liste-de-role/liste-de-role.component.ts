import { Component, OnInit } from '@angular/core';
import { RoleDto } from '../model/role-dto';
import { RoleService } from '../service/role.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-de-role',
  templateUrl: './liste-de-role.component.html',
  styleUrls: ['./liste-de-role.component.css']
})
export class ListeDeRoleComponent implements OnInit {

  roles: RoleDto[];
  constructor(private roleService: RoleService, private router: Router) { }


  ngOnInit() {
    
    this.reloadData();
  }
  reloadData() {
    this.roleService.getAll().subscribe(
      co => {
        this.roles = co;
      }
    );
  }

  getRole(id: number) {
    this.router.navigate(['role', id]);
  }

}
