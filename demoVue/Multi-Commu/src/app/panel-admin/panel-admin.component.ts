import { Component, OnInit } from '@angular/core';

import { faSignOutAlt, faUserShield } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-panel-admin',
  templateUrl: './panel-admin.component.html',
  styleUrls: ['./panel-admin.component.css']
})
export class PanelAdminComponent implements OnInit {
  user = localStorage.getItem("current_user")
  objJson = JSON.parse(this.user);
  reset: any;
  faSignOutAlt = faSignOutAlt;
  faUserShield = faUserShield;


  constructor() { }

  ngOnInit() {
    

  }

 
}
