import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpEventType, HttpHeaderResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { AlertService } from '../service/alert.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private alertService: AlertService) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      tap(evt => {
        console.log(evt);
      }),
      catchError((err: any) => {
        console.log(err)
        if (err instanceof HttpErrorResponse
          && (err.status == 403 || err.status == 401)) {
          if (err.status == 401) {
            this.alertService.addDanger('Identifiant ou mot de passe incorrect');
          }
          this.authService.logout();
        }
        if (err instanceof HttpErrorResponse
          && (err.status == 400)) {
            
          this.alertService.addDanger(err.error);
      }
        return of(err);
      }));

  }

}
