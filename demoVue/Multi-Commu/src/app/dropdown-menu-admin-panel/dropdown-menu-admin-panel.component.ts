import { Component, OnInit } from '@angular/core';
import { faTrash, faHome ,faEdit, faPlus, faUserShield, faUser, faBeer } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-dropdown-menu-admin-panel',
  templateUrl: './dropdown-menu-admin-panel.component.html',
  styleUrls: ['./dropdown-menu-admin-panel.component.css']
})
export class DropdownMenuAdminPanelComponent implements OnInit {
  faTrash = faTrash;
  faEdit = faEdit;
  faPlus = faPlus;
  faUserShield = faUserShield;
  faUser = faUser;
  faBeer = faBeer;
  faHome = faHome;

  constructor() { }

  ngOnInit() {
  }
}
