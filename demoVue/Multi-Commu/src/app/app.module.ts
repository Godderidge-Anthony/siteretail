import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FooterComponent } from './footer/footer.component';
import { TopnavbarComponent } from './topnavbar/topnavbar.component';


import { SignupComponent } from './signup/signup.component';

import { ListeDUtilisateurComponent } from './liste-d-utilisateur/liste-d-utilisateur.component';
import { UtilisateurDetailComponent } from './utilisateur-detail/utilisateur-detail.component';
import { UtilisateurUpdateComponent } from './utilisateur-update/utilisateur-update.component';
import { ListeDeRoleComponent } from './liste-de-role/liste-de-role.component';
import { RoleDetailComponent } from './role-detail/role-detail.component';
import { UtilisateurCreateComponent } from './utilisateur-create/utilisateur-create.component';
import { LoginComponent } from './login/login.component';

import { JwtModule } from '@auth0/angular-jwt';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { DropdownMenuAdminPanelComponent } from './dropdown-menu-admin-panel/dropdown-menu-admin-panel.component';



import { GestionMonCompteUtilisateurComponent } from './gestion-mon-compte-utilisateur/gestion-mon-compte-utilisateur.component';

import { GestionMonCompteDetailComponent } from './gestion-mon-compte-detail/gestion-mon-compte-detail.component';
import { GestionMonCompteUpdateComponent } from './gestion-mon-compte-update/gestion-mon-compte-update.component';
import { GestionMonCompteEditPasswordComponent } from './gestion-mon-compte-edit-password/gestion-mon-compte-edit-password.component';

import { AlertComponent } from './alert/alert.component';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { PanelAdminComponent } from './panel-admin/panel-admin.component';
import { TheSavanaComponent } from './the-savana/the-savana.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ProduitCreateComponent } from './produit-create/produit-create.component';
import { ProduitDetailComponent } from './produit-detail/produit-detail.component';
import { ProduitListComponent } from './produit-list/produit-list.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TopnavbarComponent,
    
    SignupComponent,
    ListeDUtilisateurComponent,
    UtilisateurDetailComponent,
    UtilisateurUpdateComponent,
    ListeDeRoleComponent,
    RoleDetailComponent,
    UtilisateurCreateComponent,
    LoginComponent,
    PanelAdminComponent,
    TheSavanaComponent,
    DropdownMenuAdminPanelComponent,
    
    GestionMonCompteUtilisateurComponent,
    
    
    GestionMonCompteDetailComponent,
    GestionMonCompteUpdateComponent,
    GestionMonCompteEditPasswordComponent,
    AlertComponent,
    TheSavanaComponent,
    AccueilComponent,
    ProduitCreateComponent,
    ProduitDetailComponent,
    ProduitListComponent,
   
  
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
  
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    JwtModule.forRoot({
      config: {
        // pour injecter le token dans toutes les requetes
        tokenGetter: function tokenGetter() {
          return localStorage.getItem('access_token');
        },
        // inject le token pour tous ces chemin
        whitelistedDomains: [ 'localhost:8080'],
        // n'injecte pas le token pour ce chemin
        blacklistedRoutes: ['http://localhost:8080/auth/login',
          

        ]
      }
    }),
    NgbAlertModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent],
})
export class AppModule { }
