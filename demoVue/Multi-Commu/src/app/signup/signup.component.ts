import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { faUser, faLock, faEnvelope, faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { AlertService } from '../service/alert.service';
import { RoleDto } from '../model/role-dto';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  faUser = faUser;
  faLock = faLock;
  faEnvelope = faEnvelope;
  faCalendarAlt = faCalendarAlt;
  utilisateur: UtilisateurDto;
  utilisateur2:object;
  submit:boolean = false;
  
  constructor(private utilisateurService: UtilisateurService, private router: Router, private alertService: AlertService) { }
    
    ngOnInit() {
      
      this.utilisateur = new UtilisateurDto();
      this.submit=false;
    }

 
  signup() {

    console.log("je passe dans onsubmit")
    this.utilisateurService.createUtilisateurFree(this.utilisateur)
    .subscribe(
      data => {
        console.log("je passe dans data")
        console.log(data);
        //this.utilisateur2 = data;
        this.alertService.addSuccess("le compte "+this.utilisateur.username+" à bien été créé")
        this.router.navigateByUrl('/');
      },
      error =>{
        console.log("je passe error") 
        console.log(error);
      });
  
  }

  

  modalLoginClose() {
    document.getElementById('id02').style.display = 'none';
    console.log('coucouClose');
  }

}
