import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { ListeDUtilisateurComponent } from './liste-d-utilisateur/liste-d-utilisateur.component';
import { UtilisateurDetailComponent } from './utilisateur-detail/utilisateur-detail.component';
import { UtilisateurUpdateComponent } from './utilisateur-update/utilisateur-update.component';
import { ListeDeRoleComponent } from './liste-de-role/liste-de-role.component';
import { RoleDetailComponent } from './role-detail/role-detail.component';
import { UtilisateurCreateComponent } from './utilisateur-create/utilisateur-create.component';
import { LoginComponent } from './login/login.component';
import { PanelAdminComponent } from './panel-admin/panel-admin.component';

import { GestionMonCompteUtilisateurComponent } from './gestion-mon-compte-utilisateur/gestion-mon-compte-utilisateur.component';
import { GestionMonCompteDetailComponent } from './gestion-mon-compte-detail/gestion-mon-compte-detail.component';
import { GestionMonCompteEditPasswordComponent } from './gestion-mon-compte-edit-password/gestion-mon-compte-edit-password.component';
import { GestionMonCompteUpdateComponent } from './gestion-mon-compte-update/gestion-mon-compte-update.component';
import { TheSavanaComponent } from './the-savana/the-savana.component';
import { SignupComponent } from './signup/signup.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ProduitCreateComponent } from './produit-create/produit-create.component';
import { ProduitDetailComponent } from './produit-detail/produit-detail.component';
import { ProduitListComponent } from './produit-list/produit-list.component';



const routes: Routes = [
  { path: '', redirectTo: 'TheSavana', pathMatch: 'full' },
  
  { path: 'TheSavana', component: TheSavanaComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'utilisateur', component: ListeDUtilisateurComponent },
  { path: 'utilisateur/:id', component: UtilisateurDetailComponent },
  { path: 'create', component: UtilisateurCreateComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'gestion', component: PanelAdminComponent, children: [
      { path: '', redirectTo: 'utilisateurs', pathMatch: 'full' },
      {
        path: 'utilisateurs', component: ListeDUtilisateurComponent, children: [
          { path: 'detail', component: UtilisateurDetailComponent },
          { path: 'update', component: UtilisateurUpdateComponent },
          { path: 'create', component: UtilisateurCreateComponent },
        ]
      },
      
      { path: 'roles', component: ListeDeRoleComponent },
      { path: 'role/:id', component: RoleDetailComponent },
     
    ]
  },

  { path: 'mon-compte', component: GestionMonCompteUtilisateurComponent },
  { path: 'mon-compte/detail', component: GestionMonCompteDetailComponent },
  { path: 'mon-compte/password', component: GestionMonCompteEditPasswordComponent },
  { path: 'mon-compte/edit', component: GestionMonCompteUpdateComponent },

  { path: 'gestion/utilisateurs', component: ListeDUtilisateurComponent },
  { path: 'gestion/utilisateur/detail', component: UtilisateurDetailComponent },
  
  { path: 'gestion/utilisateur/edit', component: UtilisateurUpdateComponent },
  { path: 'accueil' , component:AccueilComponent },
  { path: 'produit-create' , component:ProduitCreateComponent},
  
  { path: 'produit-list' , component:ProduitListComponent}

  
 

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
