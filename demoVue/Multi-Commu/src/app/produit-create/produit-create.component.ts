import { EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CategoryProduitdto } from '../model/categoryProduit-dto';
import { ProduitDto } from '../model/produit-dto';
import { AlertService } from '../service/alert.service';
import { CategoryProduitService } from '../service/categoryProduit.service';
import { ProduitService } from '../service/produit.service';

@Component({
  selector: 'app-produit-create',
  templateUrl: './produit-create.component.html',
  styleUrls: ['./produit-create.component.css']
})
export class ProduitCreateComponent implements OnInit {
  produit: ProduitDto = new ProduitDto();
  submitted = false;

  constructor(private produitService: ProduitService,
    private router: Router) { }

  ngOnInit() {
  }

  newEmployee(): void {
    this.submitted = false;
    this.produit = new ProduitDto();
  }

  save() {
    this.produitService.createCreateProduit(this.produit)
      .subscribe(data => console.log(data), error => console.log(error));
    this.produit = new ProduitDto();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/produits']);
  }
}


