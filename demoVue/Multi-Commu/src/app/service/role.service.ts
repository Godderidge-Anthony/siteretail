import { Injectable } from '@angular/core';
import { RoleDto } from '../model/role-dto';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

 
  private roleUrl = 'http://localhost:8080/roles/'


  roles: RoleDto[];
  role: RoleDto;
  subjectMiseAJour = new Subject<number>();

  constructor(private http: HttpClient) { }
  getAll(): Observable<any> {
    console.log(this.roleUrl);
    return this.http.get(this.roleUrl);
  }
  getUtilisateur(id: number): Observable<any> {
    console.log(this.roleUrl + id + '/');
    return this.http.get(this.roleUrl + id + '/');
  }


}
