import { Injectable } from '@angular/core';

import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CategoryProduitdto } from '../model/categoryProduit-dto';

@Injectable({
  providedIn: 'root'
})
export class CategoryProduitService {
  typedeproduitsUrl= 'http://localhost:8080/typeDeProduits/';
  //typedebieresUrl =  `${environment.backSchema}://${environment.backServer}/typeDeBieres/`;


  typedebieres: CategoryProduitdto[];
  typedebiere: CategoryProduitdto;

  subjectMiseAJour = new Subject<number>();

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    
    return this.http.get(this.typedeproduitsUrl);
  }

  getOne(value: number): Observable<any> {
    
    return this.http.get(this.typedeproduitsUrl + value + '/');
  }
  createTypeDeBiere(typedeproduit: CategoryProduitdto): Observable<object> {
    return this.http.post(this.typedeproduitsUrl, typedeproduit);
  }
  updateBiere(id: number, typeDeProduit: CategoryProduitdto): Observable<object> {
    return this.http.put(this.typedeproduitsUrl, typeDeProduit);
  }
  deleteBiere(id: number): Observable<any> {
    return this.http.delete(this.typedeproduitsUrl + id + '/');
  }

  findbyfilter(filter: number): Observable<any> {
  
    return this.http.get(this.typedeproduitsUrl + filter + '/');
  }
}
