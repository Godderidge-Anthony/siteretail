import { Injectable } from '@angular/core';
import { UtilisateurAuthDto } from '../model/utilisateur-auth-dto';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { Subject } from 'rxjs/internal/Subject';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RoleDto } from '../model/role-dto';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string;
  subjectConnexion: Subject<number>;
  currentUser: UtilisateurDto;

  constructor(private router: Router, private http: HttpClient) {
   
    this.url = 'http://localhost:8080/auth/login';
    this.subjectConnexion = new Subject<number>();
  }

  isConnected(): boolean {
    return Boolean(localStorage.getItem('isConnected'));
  }

  getCurrentUser(): UtilisateurDto {
    const userStr = localStorage.getItem('current_user');
    return JSON.parse(userStr);
  }

  login(user: UtilisateurAuthDto): Observable<boolean> {
    return new Observable(observer => {
      this.http.post(this.url, user).subscribe(res => {
        console.log("service login")
        localStorage.setItem('isConnected', 'true');
        localStorage.setItem('access_token', res['token']);
        const currentUser = new UtilisateurDto();
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(res['token']);
        currentUser.id = decodedToken.sub;
        currentUser.nom = decodedToken.username;
        currentUser.role = new RoleDto(undefined, decodedToken.roles);
        localStorage.setItem('current_user', JSON.stringify(currentUser));
        this.subjectConnexion.next();
        
        observer.next(true);
      },
        err => {
          observer.next(false);
        },
        () => {
          observer.complete();
        });
    });

  }

  logout() {
    localStorage.removeItem('isConnected');
    localStorage.removeItem('access_token');
    localStorage.removeItem('current_user');
    this.subjectConnexion.next();
    this.router.navigateByUrl('/login');
  }
}
