import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { RoleDto } from '../model/role-dto';
import { Observable } from 'rxjs';
import { RoleService } from '../service/role.service';

@Component({
  selector: 'app-utilisateur-update',
  templateUrl: './utilisateur-update.component.html',
  styleUrls: ['./utilisateur-update.component.css']
})
export class UtilisateurUpdateComponent implements OnInit {
  idUserParam: any;
  idU: string;
  utilisateur: UtilisateurDto;
  roles: Observable<any>;

  constructor(private route: ActivatedRoute,private roleService: RoleService, private router: Router,
    private utilisateurService: UtilisateurService) { }

  ngOnInit() {
 

    this.roleService.getAll().subscribe(
      types => {
        this.roles = types;
      }
    );
    this.utilisateur = new UtilisateurDto();
    this.utilisateur.role = new RoleDto();

    this.route.queryParams
    .subscribe(params => {
      console.log(params); // {order: "popular"}
      
      this.idUserParam = params;
      console.log(this.idUserParam); // popular
    });
    var strId=Object.values(this.idUserParam).toString()
    var id =parseInt(strId, 10)

    this.utilisateurService.getUtilisateur(id).subscribe(
      donnees => {
      console.log(id);
      this.utilisateurService.getUtilisateur(id)
        .subscribe(
          data => {
          console.log(data)
          this.utilisateur = data;
        }, error => console.log(error));
    });
  }


  onSubmit() {
    this.utilisateurService.updateUtilisateur(this.utilisateur)
    .subscribe(data => {
    },
      error => console.log(error));
      this.router.navigate(['gestion/utilisateurs']);
  }

  
}



