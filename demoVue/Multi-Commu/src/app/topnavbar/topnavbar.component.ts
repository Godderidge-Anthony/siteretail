import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  styleUrls: ['./topnavbar.component.css'],
})
export class TopnavbarComponent implements OnInit {

  isConnected: boolean;
  isAdmin: boolean;

  constructor(private router: Router, private authService: AuthService) {

  }

  ngOnInit() {
    this.reloadData();
  }


  reloadData() {
    this.isConnected = this.authService.isConnected();
    this.authService.subjectConnexion.subscribe(
      res => {
        this.isConnected = this.authService.isConnected();
        if (this.authService.getCurrentUser()) {
          this.isAdmin = this.authService.getCurrentUser().role.nom === "Admin";
          console.log(this.isAdmin);
        }
        
       
      }
    )
  }
  logout() {
    localStorage.clear();
    localStorage.setItem('age', '18');
    this.reloadData();

  }
}
