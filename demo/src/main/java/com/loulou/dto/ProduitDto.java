package com.loulou.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProduitDto {
	private Integer id;
	private String nom;
	private String description;
	private Double prix;
	private Integer quantite;
	private CategoryProduitDto categoryProduitDto;
	
}
