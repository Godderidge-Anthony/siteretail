package com.loulou.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@DynamicInsert
@DynamicUpdate
public class Utilisateur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ")
	private Integer id;
	private String nom;
	private String prenom;
	private String tokenSecret;
	private String password;
	private Date dateAge;
	
	private Boolean actif;
	
	@Column(unique=true)
	private String username;	
	
	@Column(unique=true)
	private String adresseMail;
	
	
	@ManyToOne
	@JoinColumn(name = "role")
	private Role role;
	
	
	
}
