package com.loulou.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.loulou.entity.CategorieProduit;


@Repository
public interface CategoryProduitRepository extends PagingAndSortingRepository<CategorieProduit, Integer>{
	public Page<CategorieProduit> findAll(Pageable pageable);

	public Optional<CategorieProduit> findByNom(String type);
	public Optional<CategorieProduit> findById(Integer id);
}
