package com.loulou.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.loulou.entity.Produit;

@Repository
public interface ProduitRepository extends PagingAndSortingRepository<Produit, Integer>{

	public Page<Produit> findAll(Pageable pageable);
	
	public Optional<Produit> findByNom(String nom);
	
	
	@Query(value = "SELECT * FROM Produit b WHERE b.nom LIKE %:nom%",
			  nativeQuery = true)
	public List<Produit>findAllproduitParNom(@Param(value = "nom") String nom);
	
	@Query(value = "SELECT * FROM Produit b WHERE b.categoryproduit = :id", 
			  nativeQuery = true)
	public List<Produit>findAllByTypeDeProduit(Integer id);
	
}
