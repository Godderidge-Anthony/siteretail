package com.loulou.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loulou.dao.CategoryProduitRepository;
import com.loulou.dao.ProduitRepository;
import com.loulou.dto.CategoryProduitDto;
import com.loulou.entity.CategorieProduit;
import com.loulou.entity.Produit;
import com.loulou.service.interf.ICategoryProduitService;

@Service
public class CategoryProduitServiceImpl implements ICategoryProduitService {
	static int INC = 20;
	public static int fin;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private CategoryProduitRepository typeDeProduitRepository;

	@Autowired
	private ProduitRepository produitRepository;

	@Override
	public Optional<CategoryProduitDto> findById(int id) {
		Optional<CategorieProduit> prod = this.typeDeProduitRepository.findById(id);
		Optional<CategoryProduitDto> res = Optional.empty();
		if (prod.isPresent()) {
			CategorieProduit p = prod.get();
			System.err.println(p.toString());
			CategoryProduitDto typeDeProduitDto = this.modelMapper.map(p, CategoryProduitDto.class);

			System.err.println(typeDeProduitDto.toString());

			res = Optional.of(typeDeProduitDto);
		}
		return res;
	}

	@Override
	public Boolean supprimerId(int id) {
		System.err.println("id de supprimer " + id);
		Optional<CategorieProduit> optionalType = this.typeDeProduitRepository.findById(id);

		if (optionalType.isPresent()) {
			System.err.println("optional apres is present " + optionalType.toString());
			List<Produit> list = this.produitRepository.findAllByTypeDeProduit(optionalType.get().getId());
			System.err.println("la taille " + list.size());
			if (list.size() > 0) {
				for (Produit produit : list) {
					produit.setCategoryProduit(CategorieProduit.builder().id(0).build());
					this.produitRepository.save(produit);

				}

			}

			this.typeDeProduitRepository.deleteById(id);
			return true;
		} else {
			System.err.println("par la pas de typedebiere");
			return false;
		}

	}

	@Override
	public Integer ajouter(CategoryProduitDto typeDeProduit) {
		String cap = typeDeProduit.getNom().substring(0, 1).toUpperCase() + typeDeProduit.getNom().substring(1);
		typeDeProduit.setNom(cap);
		CategorieProduit p = this.modelMapper.map(typeDeProduit, CategorieProduit.class);
		try {
			System.err.println("ajouter " + p.toString());
			this.typeDeProduitRepository.save(p);
		} catch (Exception e) {
			System.err.println("exception" + e.getStackTrace());

			return null;
		}
		System.err.println("id de p : " + p.getId());

		return p.getId();
	}

	@Override
	public Integer Maj(CategoryProduitDto typeDeProduit) {
		String cap = typeDeProduit.getNom().substring(0, 1).toUpperCase() + typeDeProduit.getNom().substring(1);
		typeDeProduit.setNom(cap);

		CategorieProduit p = this.modelMapper.map(typeDeProduit, CategorieProduit.class);

		try {
			System.err.println("ajouter " + p.toString());
			if (typeDeProduitRepository.existsById(typeDeProduit.getId())) {
				this.typeDeProduitRepository.save(p);
				return p.getId();
			} else {
				return 0;
			}
		} catch (Exception e) {
			System.err.println("exception" + e.getStackTrace());

			return 0;
		}

	}

	@Override
	public Optional<CategoryProduitDto> trouverParNom(String type) {
		String cap = type.substring(0, 1).toUpperCase() + type.substring(1);
		Optional<CategorieProduit> prod = this.typeDeProduitRepository.findByNom(cap);
		Optional<CategoryProduitDto> res = Optional.empty();
		if (prod.isPresent()) {
			CategorieProduit p = prod.get();
			System.err.println(p.toString());
			CategoryProduitDto typeDeProduitDto = this.modelMapper.map(p, CategoryProduitDto.class);

			System.err.println(typeDeProduitDto.toString());

			res = Optional.of(typeDeProduitDto);
		}
		return res;
	}

	@Override
	public List<CategoryProduitDto> chercherToutLesTypesDeProduit(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);

		typeDeProduitRepository.findAll(firstPageWithFiveElements);

		List<CategoryProduitDto> maliste = this.typeDeProduitRepository.findAll(firstPageWithFiveElements).stream()
				.map(e -> CategoryProduitDto.builder().id(e.getId()).nom(e.getNom()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;

	}

}
