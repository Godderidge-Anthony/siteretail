package com.loulou.service.interf;

import java.util.List;
import java.util.Optional;

import com.loulou.dto.ProduitDto;



public interface IProduitService {
	public List<ProduitDto> chercherToutLesProduits(int page);

	public Optional<ProduitDto> findById(int id);

	Boolean supprimerId(int id);
	
	public Optional<ProduitDto> trouverParnom(String nom);


	Integer ajouter(ProduitDto prod);

	public List<ProduitDto> listerParTypeDeProduit(Integer id);
	
	public List<ProduitDto> listerParNom(String nom);

	

}
