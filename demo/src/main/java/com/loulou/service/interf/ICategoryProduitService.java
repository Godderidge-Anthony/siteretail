package com.loulou.service.interf;

import java.util.List;
import java.util.Optional;

import com.loulou.dto.CategoryProduitDto;


public interface ICategoryProduitService {
	public List<CategoryProduitDto> chercherToutLesTypesDeProduit(int page);

	public Optional<CategoryProduitDto> findById(int id);

	Boolean supprimerId(int id);


	Integer ajouter(CategoryProduitDto prod);

	Integer Maj(CategoryProduitDto prod);

	public Optional<CategoryProduitDto> trouverParNom(String type);
}
