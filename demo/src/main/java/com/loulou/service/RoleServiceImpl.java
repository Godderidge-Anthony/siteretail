package com.loulou.service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loulou.dao.RoleRepository;
import com.loulou.dao.UtilisateurRepository;
import com.loulou.dto.RoleDto;
import com.loulou.entity.Role;
import com.loulou.service.interf.IRoleService;



@Service
public class RoleServiceImpl implements IRoleService {
	static int INC = 5;
	public static int fin;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;



	
	@Override
	public Optional<RoleDto> findById(int id) {
		Optional<Role> prod = this.roleRepository.findById(id);
		Optional<RoleDto> res = Optional.empty();
		if(prod.isPresent()) {
			Role p = prod.get();
			System.err.println(p.toString());
			RoleDto roleDto = this.modelMapper.map(p, RoleDto.class);
			
			System.err.println(roleDto.toString());

			res = Optional.of(roleDto);
		}
		return res;
	}
	
//	@Override
//	public Boolean supprimerId(int id) {
//		Optional<Role> prod=this.roleRepository.findById(id);
//		if(prod.isPresent()) {
//			List<Utilisateur> list=utilisateurRepository.UtilisateursByRole(prod.get().getId());
//			if(list.size()>0) {
//				for (Utilisateur utilisateur : list) {
//					utilisateur.setRole(Role.builder().Id(0).build());
//					utilisateurRepository.save(utilisateur);
//					System.err.println("suppression role avec users "+utilisateur.toString());
//				}
//				
//			}
//			
//			
//			this.roleRepository.deleteById(id);
//			return true;
//		}else {
//			System.err.println("par la");
//			return false;
//		}
//		
//	}
//	
//	
//	@Override
//	public Boolean suprimerRoleActif(int id) {
//		//je ne sais pas encore si on doit l'utiliser ou pas a voir
//		return null;
//	}
//	
//	
//	@Override
//	public Integer ajouter(RoleDto role) {
//		String cap = role.getNom().substring(0, 1).toUpperCase() + role.getNom().substring(1);
//		role.setNom(cap);
//		Role p = this.modelMapper.map(role,Role.class);
//		try {
//			System.err.println("ajouter "+p.toString());
//			this.roleRepository.save(p);			
//		} catch (Exception e) {
//			System.err.println("exception" +e.getStackTrace());
//			
//			return null;
//	}
//		System.err.println("id de p : "+p.getId());		
//		return p.getId();
//	}
//	
//	@Override
//	public Integer Maj(RoleDto role) {
//		String cap = role.getNom().substring(0, 1).toUpperCase() + role.getNom().substring(1);
//		role.setNom(cap);
//		Role p = this.modelMapper.map(role,Role.class);
//		
//		
//		try {
//			System.err.println("ajouter "+p.toString());
//			if(roleRepository.existsById(role.getId())) {
//				this.roleRepository.save(p);	
//				return p.getId();
//			}else {
//				return 0;
//			}
//		} catch (Exception e) {
//			System.err.println("exception" +e.getStackTrace());
//			
//			return 0;
//		}
//		
//	}
	


	@Override
	public List<RoleDto> chercherTousLesTypesDeRoles(int page) {
Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		
		roleRepository.findAll(firstPageWithFiveElements);
		
		List<RoleDto> maliste = this.roleRepository.findAll(firstPageWithFiveElements)
				.stream()
				.map(e->RoleDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public Optional<RoleDto> trouverParnom(String nom) {
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);
		Optional<Role> prod = this.roleRepository.findByNom(cap);
		Optional<RoleDto> res = Optional.empty();
		if(prod.isPresent()) {
			Role p = prod.get();
			System.err.println(p.toString());
			RoleDto roleDto = this.modelMapper.map(p, RoleDto.class);
			
			System.err.println(roleDto.toString());

			res = Optional.of(roleDto);
		}
		return res;
	}

	


}
