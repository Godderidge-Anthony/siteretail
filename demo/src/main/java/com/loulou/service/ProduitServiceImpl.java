package com.loulou.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loulou.dao.ProduitRepository;
import com.loulou.dto.CategoryProduitDto;
import com.loulou.dto.ProduitDto;
import com.loulou.entity.Produit;
import com.loulou.service.interf.IProduitService;

@Service
public class ProduitServiceImpl implements IProduitService {
	static int INC = 20;
	public static int fin;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ProduitRepository produitRepository;

	@Override
	public List<ProduitDto> chercherToutLesProduits(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);

		List<ProduitDto> maliste = this.produitRepository.findAll(firstPageWithFiveElements).stream()
				.map(e -> ProduitDto.builder().id(e.getId()).nom(e.getNom()).prix(e.getPrix()).quantite(e.getQuantite())
						.description(e.getDescription()).categoryProduitDto(CategoryProduitDto.builder()
								.id(e.getCategoryProduit().getId()).nom(e.getCategoryProduit().getNom()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		System.err.println("retour de la liste");
		return maliste;

	}

	@Override
	public Optional<ProduitDto> findById(int id) {
		Optional<Produit> prod = this.produitRepository.findById(id);
		Optional<ProduitDto> res = Optional.empty();
		if (prod.isPresent()) {
			Produit p = prod.get();
			System.err.println(p.toString());
			ProduitDto prodDto = this.modelMapper.map(p, ProduitDto.class);
			prodDto.setCategoryProduitDto(CategoryProduitDto.builder().id(p.getCategoryProduit().getId())
					.nom(p.getCategoryProduit().getNom()).build());
			System.err.println(prodDto.toString());

			res = Optional.of(prodDto);
		}
		return res;
	}

	@Override
	public Boolean supprimerId(int id) {
		if (this.produitRepository.existsById(id)) {
			this.produitRepository.deleteById(id);
			System.err.println("par ici");
			return true;
		} else {
			System.err.println("par la");
			return false;
		}
	}

	@Override
	public Integer ajouter(ProduitDto produit) {
		System.err.println(produit.toString());
		String cap = produit.getNom().substring(0, 1).toUpperCase() + produit.getNom().substring(1);
		produit.setNom(cap);
		Produit p = this.modelMapper.map(produit, Produit.class);
		System.err.println(p.toString());
		try {
			this.produitRepository.save(p);
		} catch (Exception e) {
			System.err.println("exception" + e.getStackTrace());
			return null;
		}
		System.err.println("produit id" + p.getId());
		return p.getId();
	}

	@Override
	public Optional<ProduitDto> trouverParnom(String nom) {
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);

		Optional<Produit> prod = this.produitRepository.findByNom(cap);
		Optional<ProduitDto> res = Optional.empty();
		if (prod.isPresent()) {
			Produit p = prod.get();
			System.err.println(p.toString());
			ProduitDto prodDto = this.modelMapper.map(p, ProduitDto.class);
			prodDto.setCategoryProduitDto(CategoryProduitDto.builder().id(p.getCategoryProduit().getId())
					.nom(p.getCategoryProduit().getNom()).build());
			System.err.println(prodDto.toString());

			res = Optional.of(prodDto);
		}
		return res;
	}

	@Override
	public List<ProduitDto> listerParTypeDeProduit(Integer id) {
		List<ProduitDto> maliste = this.produitRepository.findAllByTypeDeProduit(id).stream()
				.map(e -> ProduitDto.builder().id(e.getId()).nom(e.getNom()).prix(e.getPrix()).quantite(e.getQuantite())
						.description(e.getDescription()).categoryProduitDto(CategoryProduitDto.builder()
								.id(e.getCategoryProduit().getId()).nom(e.getCategoryProduit().getNom()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		System.err.println("retour de la liste");
		return maliste;

	}

	@Override
	public List<ProduitDto> listerParNom(String nom) {
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);

		List<ProduitDto> maliste = this.produitRepository.findAllproduitParNom(cap).stream()
				.map(e -> ProduitDto.builder().id(e.getId()).nom(e.getNom()).prix(e.getPrix()).quantite(e.getQuantite())
						.description(e.getDescription()).categoryProduitDto(CategoryProduitDto.builder()
								.id(e.getCategoryProduit().getId()).nom(e.getCategoryProduit().getNom()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		System.err.println("retour de la liste");
		return maliste;
	}

}