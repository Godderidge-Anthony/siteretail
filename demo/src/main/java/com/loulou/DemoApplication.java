package com.loulou;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.loulou.constant.AdminUserDefaultConf;

import com.loulou.dao.CategoryProduitRepository;

import com.loulou.dao.ProduitRepository;
import com.loulou.dao.RoleRepository;

import com.loulou.dao.UtilisateurRepository;
import com.loulou.entity.CategorieProduit;
import com.loulou.entity.Produit;
import com.loulou.entity.Role;

import com.loulou.entity.Utilisateur;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class DemoApplication implements WebMvcConfigurer{

	public static void main(String[] args) {
	
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.loulou.controller"))
                .build();
    }
	
		
	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
}
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedMethods("GET", "PUT", "POST", "DELETE", "PATCH");
	}
	
	@Bean
	public CommandLineRunner init(RoleRepository roleRepository, UtilisateurRepository utilisateurRepository,AdminUserDefaultConf adminUserConf,CategoryProduitRepository typeDeProduitRepository, ProduitRepository produitRepository) {
		return (String... args)->{
			List<CategorieProduit> type=(List<CategorieProduit>) typeDeProduitRepository.findAll();
			System.err.println(type.size());
			if(type.size()==0) {
				typeDeProduitRepository.save(new CategorieProduit(1,"Tampon"));
				typeDeProduitRepository.save(new CategorieProduit(2,"Electronique"));
				typeDeProduitRepository.save(new CategorieProduit(3,"Informatique"));
				produitRepository.save(new Produit(1, "Ordinateur", "Acer Predator", 5.0, 50000, typeDeProduitRepository.findById(3).get()));
				produitRepository.save(new Produit(2, "LED", "Lumiere", 7.0, 10, typeDeProduitRepository.findById(2).get()));
				produitRepository.save(new Produit(3, "Tablette", "Samsung", 8.5, 50000, typeDeProduitRepository.findById(3).get()));
				produitRepository.save(new Produit(4, "Micro", "Micro Casque", 5.0,  0, typeDeProduitRepository.findById(2).get()));
				produitRepository.save(new Produit(5, "Carte Mere", "AsusTEK 041024", 8.6, 50000, typeDeProduitRepository.findById(3).get()));
				
			}
			
			
			
			
			// ne pas oublier de bloquer la création d utilisateur avec le nom ou prenom admin
			Optional<Utilisateur> adminE = utilisateurRepository.findByUsername(adminUserConf.getUsername());
			if(! adminE.isPresent()) {
				roleRepository.save(new Role(1, "User"));
				roleRepository.save(new Role(2, "Admin"));
				utilisateurRepository.save(Utilisateur.builder()
				.nom(adminUserConf.getNom())
				.prenom(adminUserConf.getPrenom())
				.username(adminUserConf.getUsername())
				.adresseMail("tony@hotmail.fr")
				.dateAge(new Date())
				
				.actif(true)
				
				.password(passwordEncoder.encode(adminUserConf.getPassword()))
				.role(roleRepository.findById(2).get())
				.build());
			}
		};
		
	}
}
