package com.loulou.controller;



import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.loulou.dto.CategoryProduitDto;
import com.loulou.service.interf.ICategoryProduitService;

@RestController
public class ControllerCategoryProduit {
	int page;

	@Autowired
	ICategoryProduitService typeDeProduitService;


	@GetMapping(value = "/typeDeProduits")
	public List<CategoryProduitDto> listTypeDeBiere() {
		return this.typeDeProduitService.chercherToutLesTypesDeProduit(page);	
	}

	@GetMapping("/typeDeProduits/{type}")
	public ResponseEntity<?> AfficherTypeDeBiere(@PathVariable String type) {
		Optional<CategoryProduitDto> typebiereOptional=null;
		System.err.println("type : "+type);
		String regex = "[0-9]+";
		if(type.matches(regex)) {
			System.err.println("par la car id "+type);
			int id=Integer.parseInt(type);
			typebiereOptional = this.typeDeProduitService.findById(id);
		}else {
			System.err.println("par ici");
			typebiereOptional = this.typeDeProduitService.trouverParNom(type);

		}
		if (typebiereOptional.isPresent()) {

			return ResponseEntity.ok().body(typebiereOptional.get()); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun typeDeProduit a cet identifiant");
		}

	}

	@PostMapping("/typeDeProduits")
	ResponseEntity<Integer> ajouter(@Valid @RequestBody CategoryProduitDto typeDeProduitDto) throws URISyntaxException {
		if(typeDeProduitDto.getId()==null) {
			Integer result = typeDeProduitService.ajouter(typeDeProduitDto);
			return ResponseEntity.created(new URI("/typeDeProduit/" + result))
					.body(result);

		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
		}
	}

	@PutMapping("/typeDeProduits")
	ResponseEntity<Boolean> mettreAJourTypeDeProduit(
			@Valid 
			@RequestBody CategoryProduitDto prod
			) throws URISyntaxException {
		System.err.println(prod.toString());
		if(prod.getId()!=null) {
			Integer result = typeDeProduitService.Maj(prod);
			System.err.println("test service edit "+result);
			if(result!=0) {

				return ResponseEntity.ok().body(true);	
			}else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
			}

		}else {

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);

		}

	}

	@DeleteMapping("/typeDeProduits/{nom}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable String nom) {
		Boolean bool=false;
		System.err.println("bien : "+nom);
		String regex = "[0-9]+";
		if(nom.matches(regex)) {
			System.err.println("par la car id "+nom);
			int id=Integer.parseInt(nom);
			System.err.println("id : "+id);
			bool=typeDeProduitService.supprimerId(id);
		}else {
			Optional<CategoryProduitDto> produitOptional=this.typeDeProduitService.trouverParNom(nom);
			if(produitOptional.isPresent()) {
				System.err.println("par ici car nom");
				System.err.println("id : "+produitOptional.get().getId());
				bool=typeDeProduitService.supprimerId(produitOptional.get().getId());

			}
		}
		
		if(bool==true) {
			
			return ResponseEntity.ok().body(bool);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
		}
	}
}



