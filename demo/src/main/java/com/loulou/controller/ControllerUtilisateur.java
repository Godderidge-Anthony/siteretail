package com.loulou.controller;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.loulou.constant.AdminUserDefaultConf;
import com.loulou.dto.RoleDto;
import com.loulou.dto.UtilisateurDto;
import com.loulou.service.interf.IRoleService;
import com.loulou.service.interf.IUtilisateurService;

@RestController
public class ControllerUtilisateur {
	int page;

	@Autowired
	IUtilisateurService utilisateurService;

	@Autowired
	IRoleService roleService;

	@Autowired
	private AdminUserDefaultConf adminUserDefaultConf;

	@PreAuthorize("hasAuthority('Admin')")
	@GetMapping(value = "/utilisateurs")
	public List<UtilisateurDto> listUtilisateur() {
		return this.utilisateurService.chercherTousLesUtilisateurs(page);
	}

	
	@GetMapping("/utilisateurs/{user}")
	public ResponseEntity<?> AfficherUtilisateur(@PathVariable String user) {
		Optional<UtilisateurDto> utilisateurOptional = null;
		System.err.println("user : " + user);
		String regex = "[0-9]+";
		if (user.matches(regex)) {
			System.err.println("par la car id " + user);
			int id = Integer.parseInt(user);
			utilisateurOptional = this.utilisateurService.findById(id);
		} else {
			utilisateurOptional = this.utilisateurService.trouverParUsername(user);
		}
		if (utilisateurOptional.isPresent()) {
			System.out.println("retour de l'utilisateur " + utilisateurOptional.get().getPassword());

			return ResponseEntity.ok().body(utilisateurOptional.get());
		} else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}
	}

	@PreAuthorize("hasAuthority('Admin')")
	@GetMapping("/utilisateurs/role/{user}")
	public ResponseEntity<?> AfficherUtilisateurParRole(@PathVariable String user) {
		List<UtilisateurDto> utilisateurOptional = null;
		System.err.println("user : " + user);
		String regex = "[0-9]+";
		if (user.matches(regex)) {
			System.err.println("par la car id " + user);
			int id = Integer.parseInt(user);
			Optional<RoleDto> optionalRole = this.roleService.findById(id);
			if (optionalRole.isPresent()) {
				utilisateurOptional = this.utilisateurService.listerParRole(id);
				return ResponseEntity.ok().body(utilisateurOptional);
			}

		} else {
			Optional<RoleDto> optionalRole = this.roleService.trouverParnom(user);
			if (optionalRole.isPresent()) {
				utilisateurOptional = this.utilisateurService.listerParRole(optionalRole.get().getId());
				return ResponseEntity.ok().body(utilisateurOptional);
			}
		}

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
	}

	@PreAuthorize("hasAuthority('Admin')")
	@PostMapping("/utilisateurs")
	ResponseEntity<Object> ajouter(@Valid @RequestBody UtilisateurDto utilisateurDto) throws URISyntaxException {
		Date compare = new Date();
		long verif = compare.getTime() - utilisateurDto.getDateAge().getTime();
		if (Math.floor(verif / (1000 * 3600 * 24) / 365.25) < 18) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("age inferieur à 18 ans");
		}

		String pattern = "[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})";
		if (!utilisateurDto.getAdresseMail().matches(pattern)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("le format de votre adresse mail n'est pas correcte");
		}
		if (utilisateurDto.getId() == null) {
			if (utilisateurDto.getRole() == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
			} else if (utilisateurDto.getNom().equalsIgnoreCase(adminUserDefaultConf.getNom())
					|| utilisateurDto.getPrenom().equalsIgnoreCase(adminUserDefaultConf.getPrenom())) {

				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
			}
			System.out.println("le user qu55555e tu m'envoies " + utilisateurDto);
			Integer result = utilisateurService.ajouter(utilisateurDto);
			if (result > 0) {
				return ResponseEntity.ok(utilisateurDto);

			} else {

				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("username deja utilisé");

			}

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
		}
	}

	@PostMapping("/utilisateursFree")
	ResponseEntity<Object> ajouterFree(@Valid @RequestBody UtilisateurDto utilisateurDto) throws URISyntaxException {
		System.out.println("je passe dans le free post");
		Date compare = new Date();
		long verif = compare.getTime() - utilisateurDto.getDateAge().getTime();
		if (Math.floor(verif / (1000 * 3600 * 24) / 365.25) < 18) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("age inferieur à 18 ans");
		}
		String pattern = "[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})";
		if (!utilisateurDto.getAdresseMail().matches(pattern)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("le format de votre adresse mail n'est pas correcte");
		}

		if (utilisateurDto.getId() == null) {
			if (utilisateurDto.getNom().equalsIgnoreCase(adminUserDefaultConf.getNom())
					|| utilisateurDto.getPrenom().equalsIgnoreCase(adminUserDefaultConf.getPrenom())) {

				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
			}
			System.out.println("le user que tu m'envoies " + utilisateurDto);
			Integer result = utilisateurService.ajouter(utilisateurDto);
			if (result == -1) {
				// String body="username deja existant";
				System.out.println("ergergergergergergergergergergergergergergergergergegerrgergergergerg");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("username deja existant");
			} else {
				System.out.println("okay!!!!!!!!");

				return ResponseEntity.ok().body(utilisateurDto);
			}

		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
		}

	}

	
	@PutMapping("/utilisateurs")
	ResponseEntity<Object> mettreAJourUtilisateur(@Valid @RequestBody UtilisateurDto prod) throws URISyntaxException {
		String pattern = "[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})";
		if (!prod.getAdresseMail().matches(pattern)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("le format de votre adresse mail n'est pas correcte");
		}

		System.err.println(prod.toString());
		if (prod.getId() != null) {
			Integer result = utilisateurService.Maj(prod);
			System.err.println("test service edit " + result);
			if (result != -1) {

				return ResponseEntity.ok().body(this.utilisateurService.findById(prod.getId()).get());
			} else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(this.utilisateurService.findById(prod.getId()).get());
			}

		} else {

			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(this.utilisateurService.findById(prod.getId()).get());

		}

	}

	@PreAuthorize("hasAuthority('Admin')")
	@DeleteMapping("/utilisateurs/{user}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable String user) {
		Boolean bool = false;
		System.err.println("user : " + user);
		String regex = "[0-9]+";
		if (user.matches(regex)) {
			System.err.println("par la car id " + user);
			int id = Integer.parseInt(user);
			bool = utilisateurService.supprimerId(id);

		} else {
			Optional<UtilisateurDto> utilisateurOptional = this.utilisateurService.trouverParUsername(user);
			if (utilisateurOptional.isPresent()) {
				System.err.println("par ici car nom");
				UtilisateurDto u = utilisateurOptional.get();
				bool = utilisateurService.supprimerId(u.getId());

			}
		}

		System.err.println(bool);
		if (bool == true) {

			return ResponseEntity.ok().body(bool);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
		}

	}

}
