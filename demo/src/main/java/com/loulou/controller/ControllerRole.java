package com.loulou.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.loulou.dto.RoleDto;
import com.loulou.service.interf.IRoleService;



@RestController
public class ControllerRole {
	int page;

	@Autowired
	IRoleService roleService;
	
	@PreAuthorize("hasAuthority('Admin')")
	@GetMapping(value = "/roles")
	public List<RoleDto> listRole() {
		return this.roleService.chercherTousLesTypesDeRoles(page);	
	}
	@PreAuthorize("hasAuthority('Admin')")
	@GetMapping("/roles/{role}")
	public ResponseEntity<?> AfficherRole(@PathVariable String role) {
		Optional<RoleDto> roleOptional=null;
		System.err.println("user : "+role);
		String regex = "[0-9]+";
		if(role.matches(regex)) {
			System.err.println("par la car id "+role);
			int id=Integer.parseInt(role);
			roleOptional = this.roleService.findById(id);
		}else {
			roleOptional=this.roleService.trouverParnom(role);

		}
		if (roleOptional.isPresent()) {

			return ResponseEntity.ok().body(roleOptional.get()); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun role a cet identifiant");
		}

	}
	
	
	
//	@PostMapping("/roles")
//	ResponseEntity<Integer> ajouter(@Valid @RequestBody RoleDto roleDto) throws URISyntaxException {
//		if(roleDto.getId()==null) {
//			Integer result = roleService.ajouter(roleDto);
//			return ResponseEntity.created(new URI("/role/" + result))
//					.body(result);
//			
//		}else {
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
//		}
//    }
//	
//	@PutMapping("/roles")
//	ResponseEntity<Boolean> mettreAJourRole(
//			@Valid 
//			@RequestBody RoleDto prod
//			) throws URISyntaxException {
//		System.err.println(prod.toString());
//		if(prod.getId()!=null) {
//			Integer result = roleService.Maj(prod);
//			System.err.println("test service edit "+result);
//			if(result!=0) {
//				
//				return ResponseEntity.ok().body(true);	
//			}else {
//				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
//			}
//			
//		}else {
//			
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
//			
//		}
//
//	}
//	
//	@DeleteMapping("/roles/{role}")
//	public ResponseEntity<Boolean> deleteGroup(@PathVariable String role) {
//		Optional<RoleDto> roleOptional=null;
//		Boolean bool=false;
//		System.err.println("user : "+role);
//		String regex = "[0-9]+";
//		if(role.matches(regex)) {
//			System.err.println("par la car id "+role);
//			int id=Integer.parseInt(role);
//			
//			bool=roleService.supprimerId(id);
//			
//		}else {
//			roleOptional=this.roleService.trouverParnom(role);
//			if(roleOptional.isPresent()) {
//				
//				bool=roleService.supprimerId(roleOptional.get().getId());
//			}
//
//		}
//	
//		System.err.println(bool);
//		if(bool==true) {
//			
//			return ResponseEntity.ok().body(bool);
//		}else {
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
//		}
//		
//	}
//	
	

}

