package com.loulou.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.loulou.dto.CategoryProduitDto;
import com.loulou.dto.ProduitDto;


import com.loulou.service.interf.ICategoryProduitService;
import com.loulou.service.interf.IProduitService;


@RestController
public class ControllerProduit {
	int page;

	@Autowired
	IProduitService produitService;
	@Autowired
	ICategoryProduitService typeProduitService;


	@GetMapping(value = "/produits")
	public List<ProduitDto> listProduit() {
		return this.produitService.chercherToutLesProduits(page);	
	}

	@GetMapping("/produits/{user}")
	public ResponseEntity<?> AfficherProduit(@PathVariable String user) {
		List<ProduitDto> ListproduitOptional=null;
		System.err.println("user : "+user);
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			System.err.println("par la car id "+user);
			int id=Integer.parseInt(user);
			ListproduitOptional= new ArrayList<ProduitDto>();
			ListproduitOptional.add(this.produitService.findById(id).get());
		}else {
			
			ListproduitOptional=this.produitService.listerParNom(user);
		}
			

		if (ListproduitOptional.size()>0) {

			return ResponseEntity.ok().body(ListproduitOptional); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun produit a cet identifiant");
		}

	}
	
	@GetMapping("/produit/{user}")
	public ResponseEntity<?> AfficherProduit1(@PathVariable String user) {
		Optional<ProduitDto> produitOptional=null;
		System.err.println("user : "+user);
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			System.err.println("par la car id "+user);
			int id=Integer.parseInt(user);
			produitOptional= this.produitService.findById(id);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("aucun produit a cet identifiant");
		}
		
			

		if (produitOptional.isPresent()) {

			return ResponseEntity.ok().body(produitOptional.get()); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun produit a cet identifiant");
		}

	}
	

	@Transactional
	@PreAuthorize("hasAuthority('Admin')")
	@PostMapping("/produits")
	ResponseEntity<Integer> ajouter(@Valid @RequestBody ProduitDto produitDto) throws URISyntaxException {
		if(produitDto.getId()==null) {
			System.out.println("le produit que tu m'envoies "+produitDto);
			Integer result = produitService.ajouter(produitDto);
			return ResponseEntity.created(new URI("/produit/" + result))
					.body(result);

		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);

		}
	}
	@PreAuthorize("hasAuthority('Admin')")
	@PutMapping("/produits")
	ResponseEntity<Boolean> mettreAJourProduit(
			@Valid 
			@RequestBody ProduitDto prod
			) throws URISyntaxException {
		System.err.println(prod.toString());
		if(prod.getId()!=null) {
			Integer result = produitService.ajouter(prod);
			System.err.println(result);
			return ResponseEntity.ok().body(true);	


		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);

	}
	@PreAuthorize("hasAuthority('Admin')")
	@DeleteMapping("/produits/{produit}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable String produit) {
		Boolean bool=false;
		System.err.println("user : "+produit);
		String regex = "[0-9]+";
		if(produit.matches(regex)) {
			System.err.println("par la car id "+produit);
			int id=Integer.parseInt(produit);
		System.err.println("id : "+id);
		bool=produitService.supprimerId(id);
		}else {
			Optional<ProduitDto> produitOptional=this.produitService.trouverParnom(produit);
			if(produitOptional.isPresent()) {
				System.err.println("par ici car nom");
				ProduitDto b=produitOptional.get();
				bool=this.produitService.supprimerId(b.getId());
			}
			
		}
			

		System.err.println(bool);
		if(bool==true) {

			return ResponseEntity.ok().body(bool);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
		}

	}



}

